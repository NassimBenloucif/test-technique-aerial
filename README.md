# Test Technique Aerial

Test technique pour le recrutement d'un développeur junior chez Aerial Group

## Environnement

L'application devra tourner sur Php 7.4 et Symfony 5. Côté BDD, utilisez MySQL 5.7

## Objectifs

- Sur la page d'acceuil, lister les produits de la base de données. Ajouter un tri par prix croissant / décroissant.
- Mettre en place un bouton ajout de produit sous forme de modal, comprenant un formulaire de création d'un produit avec les champs : ID, nom, description, prix HT, TVA, image  (une seule image par produit)
- Utiliser JS vanilla ou Jquery au choix pour rendre le contenu dynamique ( évitez le rechargement de page )

## Bonus si vous avez le temps
- Mettre une pagination sur les produits (ex: 2 ou 3 maximum par pages)
- Barre de recherche de produit par nom

## A savoir

La qualité du design / intégration n'est pas importante . Vous serez principalement évaluer sur la qualité et la logique du code. Le gitflow est aussi a prendre en compte, ainsi que les bonnes pratiques de développement. A date de réception du test , vous avez deux jours pour le renvoyer.

Pour toutes questions, n'hésitez pas a me contacter par mail : qmoreau28@gmail.com ou par sms : 06 70 22 90 72.


# Bon courage !
