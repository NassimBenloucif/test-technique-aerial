<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Product>
 *
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function add(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
    
    public function getByOrder(string $order, int $page):array{
        $qb = $this->createQueryBuilder('p')
            ->setMaxResults('3')
            ->setFirstResult($page*3)
            ->orderBy('p.price', $order);
        
        $query = $qb->getQuery();
        return $query->execute();
    }
    
    public function getPaged(int $page):array{
        $qb = $this->createQueryBuilder('p')
            ->setMaxResults('3')
            ->setFirstResult($page*3);
        
        $query = $qb->getQuery();
        return $query->execute();
    }
    
    public function countAll():int{
        $qb = $this->createQueryBuilder('p')
                    ->select('count(p.id)');
        
        $query = $qb->getQuery()
                    ->getSingleScalarResult();
        return $query;
    }
    
    public function searchByName(string $term):array{

        $qb = $this->createQueryBuilder('p')
                    ->where('p.name LIKE :nom')
                    ->setParameter('nom', '%'.$term.'%')
                    ->setMaxResults('3');
        
        $query = $qb->getQuery();

        return $query->execute();
    }

//    /**
//     * @return Product[] Returns an array of Product objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Product
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
