<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Product;

class ProduitController extends AbstractController
{
    #[Route('/produit/ProductSearch', name: 'ProductSearch')]
    public function search(ManagerRegistry $doctrine, Request $request): Response
    {
        $term = $request->get('search');

        $repo= $doctrine->getRepository(Product::class);
        $products = $repo->searchByName($term);
        return $this->json($products);
        
    }
    
    /********************************** */
    #[Route('/produit/ProductSearchList', name: 'ProductSearchList')]
    public function getList(ManagerRegistry $doctrine): Response
    {
        $repo= $doctrine->getRepository(Product::class);
        $products = $repo->findAll();
        return $this->json($products);
    }
    
    /********************************** */
    #[Route('/produit/all', name: 'all_products')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $repo= $doctrine->getRepository(Product::class);
        $products = $repo->findby([],[],3,0);
        return $this->json($products);
    }
    
    /********************************* */
    #[Route('/produit/order', name: 'getPage')]
    public function getPage(ManagerRegistry $doctrine, Request $request): Response
    {
        $order = $request->get('orderBy');
        $page = $request->get('pagination');

        $repo= $doctrine->getRepository(Product::class);
        if($order != ''){
            $products = $repo->getByOrder($order, $page);
        }else{
            $products = $repo->getPaged($page);
        }
        
        return $this->json($products);
    }

    /********************************** */
    #[Route('/produit/count', name: 'count_products')]
    public function countProducts(ManagerRegistry $doctrine): Response
    {

        $repo= $doctrine->getRepository(Product::class);
        $count = $repo->countAll();
        $pages = ceil($count/3);
        
        return $this->json($pages);
    }

    /********************************* */
    #[Route('/produit/add', name: 'add_product')]
    public function add(ManagerRegistry $doctrine, Request $request): Response
    {
        $entityManager = $doctrine->getManager();

        $price = $request->get('Price');
        $nomProduit = $request->get('NomProduit');

        $produit = new product();
        $produit->setName($nomProduit);
        $produit->setPrice($price);

        $entityManager->persist($produit);
        $entityManager->flush();
        
        return new Response(
            'Data saved.'
        );
    }
}
